#!/bin/sh
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf

pacman --noconfirm -Sy archlinux-keyring

loadkeys uk

timedatectl set-ntp true

printf '\033c'
lsblk
echo -n "Enter drive name: "
read -r drive
cfdisk $drive
sleep 2
printf '\033c'
lsblk
echo -n "Enter EFI/boot partition: "
read -r efipartition
mkfs.vfat -F 32 $efipartition
sleep 2
printf '\033c'
lsblk
echo -n "Enter swap partition (leave empty for no swap):"
read -r swappartition
sleep 2
mkswap $swappartition
sleep 2
printf '\033c'
lsblk
echo -n "Enter your root partition: "
read -r rootpartition
mkfs.ext4 $rootpartition
sleep 2
printf '\033c'
mount $rootpartition /mnt
mount --mkdir $efipartition /mnt/boot
swapon $swappartition
sleep 2

pacstrap /mnt base base-devel linux linux-firmware linux-headers vim neovim

genfstab -U /mnt >> /mnt/etc/fstab

sed '1,/^#p2start$/d' $(basename $0) > /mnt/archinstall2.sh
chmod +x /mnt/archinstall2.sh

arch-chroot /mnt ./archinstall2.sh
exit

#p2start
# Clear screen
printf '\033c'

ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

hwclock --systohc

sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf

sed -i 's/#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen
# Generate locale
locale-gen
# Set other locale
echo "LANG=en_GB.UTF-8" > /etc/locale.conf
echo "KEYMAP=uk" > /etc/vconsole.conf

echo -n "Enter your hostname: "
read -r hostname
echo $hostname > /etc/hostname

echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts

passwd

# Install bootloader packages.
pacman --noconfirm -S grub efibootmgr
# Setup/install bootloader.
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-install --target=x86_64-efi --efi-directory=/boot --removable
sed -i "s/^GRUB_GFXMODE=auto$/GRUB_GFXMODE=1920x1080/" /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

echo "---------------------------------------"
echo "| select processor make for microcode |"
echo "|=====================================|"
echo "| For Intel, enter i                  |"
echo "| For AMD, enter a                    |"
echo "| Leave blank for both                |"
echo "---------------------------------------"
echo -n "Your processor option: "
read -r processor
if [ "$processor" == "a" ] ; then
  pacman -S --noconfirm amd-ucode
elif [ "$processor" == "i" ] ; then
  pacman -S --noconfirm intel-ucode
else
  pacman -S --noconfirm intel-ucode amd-ucode
fi

echo "-----------------------------------------------------"
echo "| Do you want nvidia, amd or intel drivers? (n/a/i) |"
echo "-----------------------------------------------------"
echo -n "Your response: "
read -r video
if [ "$video" == "n" ] ; then
  pacman -S --noconfirm nvidia nvidia-utils nvtop
elif [ "$video" == "i" ] ; then
  pacman -S --noconfirm xf86-video-intel mesa
elif [ "$video" == "a" ] ; then
  pacman -S --noconfirm xf86-video-amdgpu
fi

pacman -S --noconfirm --disable-download-timeout noto-fonts noto-fonts-emoji \
    noto-fonts-cjk ttf-jetbrains-mono ttf-font-awesome mpv zathura \
    zathura-pdf-mupdf ffmpeg fzf man-db fd emacs-wayland mpd ncmpcpp \
    xdg-user-dirs libconfig pipewire alsa-utils wireplumber polkit-gnome \
    aria2 jq mako libnotify dosfstools ntfs-3g wl-clipboard hyprland \
    xdg-desktop-portal-hyprland rustup networkmanager foot polkit \
    wpa_supplicant dhcpcd zip unzip unrar papirus-icon-theme arc-gtk-theme \
    capitaine-cursors lxappearance wofi rsync firefox zsh starship ripgrep eza

systemctl enable NetworkManager.service

echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

echo "-----------------------"
echo "|    Creating user    |"
echo "-----------------------"
echo -n "Enter your username: "
read -r username
useradd -m -G wheel $username
passwd $username
