rustup default stable
mkdir ~/.source
cd ~/.source
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

paru -Sy acpi alsa-utils arduino base base-devel bind blueman bluez-utils breeze brightnessctl \
    btop cabal-install capitaine-cursors cups-pdf dash deno direnv discord docker docker-compose \
    editorconfig-core-c egl-wayland element-desktop eza fd firefox firefox-developer-edition flake8 \
    flatpak foot fprintd fuzzel fwupd gammastep glow gopls gource gum gwenview hplip jupyterlab krita \
    kvantum-theme-materia kwallet-pam ledger libguestfs libreoffice-fresh libva-utils linux-zen maim \
    mako man-db mpc mpd mpv nano ncmpcpp ncspot neovim ntp nushell obs-studio onefetch otf-overpass \
    papirus-icon-theme polkit-gnome postgresql pyright python-isort python-livereload python-pipenv \
    qbittorrent qt5-xcb-private-headers qt6ct ripgrep rust-analyzer signal-desktop starship stylelint \
    swappy swayidle swaylock syncthing texlive-bibtexextra texlive-context texlive-fontsextra texlive-fontutils \
    texlive-formatsextra texlive-games texlive-humanities texlive-luatex texlive-mathscience texlive-metapost \
    texlive-music texlive-pstricks texlive-publishers texlive-xetex thunar tidy tmux ttc-iosevka-aile \
    ttf-jetbrains-mono typescript-language-server unrar unzip usbutils vdpauinfo virt-manager virt-viewer \
    wireshark-qt xdg-user-dirs xorg-xhost xorg-xlsclients yarn zip zola zoxide zsh \
    android-studio astrojs-language-server auto-cpufreq boop-gtk-bin carapace cava eww-wayland \
    fnm ghcup-hs-bin grimblast-git gtk-theme-material-black howdy hyprpicker jetbrains-toolbox \
    joshuto keybase-bin lsix-git marp-cli-bin mermaid-cli mods nomachine nwg-look-bin oculante \
    pandoc-bin paru pfetch python-dlib rxfetch spotify swww ticktick tp_smapi-dkms \
    tradingview vencord-desktop-git waybar-hyprland wlr-randr wofi-emoji \
    wordnet-cli wordnet-common youtube-music-bin

rm -rf ~/.emacs.d
rm -rf ~/.config/emacs
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs
# Also clone my configuration
git clone https://github.com/hegde-atri/doom-emacs-config ~/.config/doom
~/.config/emacs/bin/doom install




















